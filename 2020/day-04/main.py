import re

neededFields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

class Passport():
    def __init__(self, passport_str):
        self.passport = passport_str
        self.byr = re.search(r"byr:(\d{4})\b", self.passport)
        self.iyr = re.search(r"iyr:(\d{4})\b", self.passport)
        self.eyr = re.search(r"eyr:(\d{4})\b", self.passport)
        self.hgt = re.search(r"hgt:(\d+)(cm|in)\b", self.passport)
        self.hcl = re.search(r"hcl:#([0-9a-f]{6})\b", self.passport)
        self.elc = re.search(r"ecl:(amb|blu|brn|gry|grn|hzl|oth)\b", self.passport)
        self.pid = re.search(r"pid:\d{9}\b", self.passport)
    
    def is_valid(self):
        count = 0

        for field in neededFields:
            if field in self.passport:
                count += 1
        
        if count == len(neededFields):
            return True

        return False
    
    def is_valid2(self):
        if None in (self.byr, self.iyr, self.eyr, self.hgt, self.hcl, self.elc, self.pid):
            return False
        
        if int(self.byr.group(1)) < 1920 or int(self.byr.group(1)) > 2002:
            return False
        if int(self.iyr.group(1)) < 2010 or int(self.iyr.group(1)) > 2020:
            return False
        if int(self.eyr.group(1)) < 2020 or int(self.eyr.group(1)) > 2030:
            return False
        if self.hgt.group(2) == "cm" and (int(self.hgt.group(1)) < 150 or int(self.hgt.group(1)) > 193):
            return False
        if self.hgt.group(2) == "in" and (int(self.hgt.group(1)) < 59 or int(self.hgt.group(1)) > 76):
            return False
        
        return True
        
    
    def __repr__(self):
        return self.passport
    



if __name__ == '__main__':
    part1 = part2 = 0
    with open('input.txt') as f:
        passports = f.read().strip().split("\n\n")

    for passport_str in passports:
        passport = Passport(passport_str)
        if passport.is_valid():
            part1 += 1
            if passport.is_valid2():
                part2 += 1
        


    print(f"Part 1: {part1}")
    print(f"Part 2: {part2}")
