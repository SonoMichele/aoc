def countTrees(lines, moveX, moveY):
    h = len(lines)
    w = len(lines[0])
    trees = 0

    x = y = 0
    for y in range(0, h, moveY):
        if lines[y][x] == "#":
            trees += 1

        x = (x+moveX) % w

    return trees


if __name__ == '__main__':
    with open('input.txt', 'r') as f:
        lines = [l.rstrip("\n") for l in f.readlines()]
    
    part1 = countTrees(lines, 3, 1)
    print(f"Part 1: {part1}")

    part2 = (
        countTrees(lines, 1, 1) *
        countTrees(lines, 3, 1) *
        countTrees(lines, 5, 1) *
        countTrees(lines, 7, 1) *
        countTrees(lines, 1, 2)      
    )
    print(f"Part 2: {part2}")