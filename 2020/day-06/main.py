def countYes(answers: str) -> int:
    found = list()

    for c in answers:
        if c not in found and c != ':':
            found.append(c)
    
    return len(found)


def countYes2(answers: str) -> int:
    p_answers = answers.split(':')
    found = dict()
    yes = 0

    if len(p_answers) == 1:
        return len(answers)
    
    for p_answer in p_answers:
        for c in p_answer:
            found[c] = found.get(c, 0) + 1
    
    for key in found.keys():
        if found[key] == len(p_answers):
            yes += 1
    
    return yes


if __name__ == '__main__':
    with open('input.txt') as f:
        answers = f.read().strip().replace('\n', ':').split("::")
    
    part1 = part2 = 0
    
    for answer in answers:
        part1 += countYes(answer)
        part2 += countYes2(answer)
    
    print(f"Part 1: {part1}")
    print(f"Part 2: {part2}")
