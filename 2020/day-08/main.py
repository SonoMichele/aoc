class Instruction():
    def __init__(self, instruction_str: str):
        self.op = instruction_str[:3]
        self.value = instruction_str[3:]

    def __repr__(self):
        return f'{self.op}: {self.value}'


def run_until_loop_or_end(instructions: list) -> tuple:
    pc = 0  # program counter
    seenLines = []
    acc = 0
    while pc < len(instructions) and pc not in seenLines:
        seenLines.append(pc)
        instruction = Instruction(instructions[pc])
        if instruction.op == "jmp":
            pc += int(instruction.value)
        elif instruction.op == "acc":
            acc += int(instruction.value)
            pc += 1
        else:
            pc += 1

    return acc, pc in seenLines


def part1(instructions: list) -> int:
    acc, _ = run_until_loop_or_end(instructions)
    return acc


def part2(instructions: list) -> int:
    pc = 0  # progam counter
    infinite = True
    counter = 0
    while pc < len(instructions) and infinite:
        copy = instructions.copy()
        instruction = Instruction(copy[pc])
        if instruction.op == "jmp":
            copy[pc] = copy[pc].replace("jmp", "nop")
        elif instruction.op == "nop":
            copy[pc] = copy[pc].replace("nop", "jmp")
        counter, infinite = run_until_loop_or_end(copy)
        pc += 1

    return counter


if __name__ == '__main__':
    with open('input.txt') as f:
        instructions = f.read().replace(' ', '').strip().split('\n')

    print(f'Part 1: {part1(instructions)}')
    print(f'Part 2: {part2(instructions)}')
