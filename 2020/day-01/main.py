with open('input.txt') as f:
    input = f.readlines()


# part 1
for i in input:
    for k in input:
        if int(i) + int(k) == 2020:
            print(f'Answer to part 1: {int(i)*int(k)}')

# part 2
for i in input:
    for k in input:
        for j in input:
            if int(i) + int(k) + int(j) == 2020:
                print(f'Answer to part 2: {int(i)*int(k)*int(j)}')
