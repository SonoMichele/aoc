checked = {'shiny gold': True, 'other': False}
checked2 = {'other': 0}


def contain_shiny(bag, rules):
    if bag in checked:
        return checked[bag]

    for b in rules[bag]:
        if contain_shiny(b[1], rules):
            checked[bag] = True
            return True

    checked[bag] = False
    return False


def count_bags(bag, rules):
    if bag in checked2:
        return checked2[bag]
    n = 1
    for b in rules[bag]:
        if b[0].isnumeric():
            n += int(b[0]) * count_bags(b[1], rules)
        else:
            n += count_bags(b[1], rules)

    checked2[bag] = n
    return n


if __name__ == '__main__':
    with open("input.txt") as f:
        rules = f.read().strip().replace('.', '').split("\n")

    rules_dict = {}
    for rule in rules:
        rule = rule.rstrip()
        outer, inner = rule.split(' bags contain')
        inner = inner.split(',')
        inside = []
        for con in inner:
            inside.append((con[:2].strip(), con.strip()[2:-4].strip()))

        rules_dict[outer] = inside

    found = 0
    for bag in rules_dict.keys():
        if contain_shiny(bag, rules_dict):
            found += 1

    print(f'Part 1: {found-1}')
    print(f'Part 2: {count_bags("shiny gold", rules_dict) - 1}')
