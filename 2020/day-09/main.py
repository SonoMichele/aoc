def findSum(preamble: list, needed: int) -> bool:
    for i in preamble:
        for j in preamble:
            if i != j and i + j == needed:
                return True

    return False


def part1(output: list) -> int:
    for i in range(25, len(output)):
        if not findSum(output[i-25:i], output[i]):
            return output[i]


def part2(output: list, needed: int) -> int:
    for i in range(0, len(output)):
        contiguous_r = [output[i], output[i+1]]
        k = i + 2
        res = 0
        while res < needed:
            res = 0
            for j in contiguous_r:
                res += j
            if res == needed:
                return min(contiguous_r) + max(contiguous_r)
            contiguous_r.append(output[k])
            k += 1


if __name__ == '__main__':
    with open('input.txt') as f:
        output = f.read().strip().split("\n")

    output = [int(i) for i in output]

    print(f'Part 1: {part1(output)}')
    print(f'Part 2: {part2(output, part1(output))}')
