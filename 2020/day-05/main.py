def findSeat(r: int, boarding_pass: str) -> int:
    seats = [i for i in range(0, r+1)]

    for c in boarding_pass:
        if c in ('F', 'L'):
            seats = seats[:int(len(seats)/2)]
        elif c in ('B', 'R'):
            seats = seats[int(len(seats)/2):]
    
    return seats[0]


def get_seat_id(boarding_pass: str) -> int:
    row = findSeat(127, boarding_pass[:7])
    col = findSeat(7, boarding_pass[7:])
    seat_id = (row * 8) + col

    return seat_id


if __name__ == '__main__':
    with open('input.txt') as f:
        boarding_passes = f.readlines()

    my_seat = 0

    seats_ids = sorted([get_seat_id(boarding_pass) for boarding_pass in boarding_passes])
    
    highest = seats_ids[-1]

    for i in range(0, len(seats_ids)):
        seat_id = seats_ids[i]
        if seat_id + 1 != seats_ids[i+1] and seat_id + 2 == seats_ids[i+1]:
            my_seat = seat_id + 1
            break

    
    print(f"Part 1: {highest}")
    print(f"Part 2: {my_seat}")
