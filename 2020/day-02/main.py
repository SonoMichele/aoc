class Password():
    def __init__(self, line):
        self.min = int(line.split('-')[0])
        self.max = int(line.split('-')[1].split()[0])
        self.char = line.split(':')[0].split()[1]
        self.passwd = line.split(':')[1].split()[0]
    

    def isValid(self):
        count = 0

        for c in self.passwd:
            if c == self.char:
                count += 1

        if count >= self.min and count <= self.max:
            return True
        
        return False


    def __repr__(self):
        return f"Min: {self.min}; Max: {self.max}; Char: {self.char}; Password: {self.passwd}"


class Password2():
    def __init__(self, line):
        self.pos1 = int(line.split('-')[0])-1
        self.pos2 = int(line.split('-')[1].split()[0])-1
        self.char = line.split(':')[0].split()[1]
        self.passwd = line.split(':')[1].split()[0]
    

    def isValid(self):
        found = False
        if self.passwd[self.pos1] == self.char:
            found = True
        
        if self.passwd[self.pos2] == self.char:
            if found:
                return False
            found = True
        
        return found



    def __repr__(self):
        return f"Pos1: {self.pos1}; Pos2: {self.pos2}; Char: {self.char}; Password: {self.passwd}"


def main():
    countPart1 = 0
    countPart2 = 0

    with open('input.txt') as f:
        input = f.readlines()
    
    for line in input:
        password = Password(line)
        password2 = Password2(line)
        if password.isValid():
            countPart1 += 1
        if password2.isValid():
            countPart2 += 1
    
    print(f'Answer part 1: {countPart1}')
    print(f'Answer part 2: {countPart2}')


if __name__ == '__main__':
    main()