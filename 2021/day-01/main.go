package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

func part1(input []int) int {
	inc := 0
	prev := 0
	for i := 0; i < len(input); i++ {
		if i == 0 {
			prev = input[0]
			continue
		}
		if prev < input[i] {
			inc++
		}
		prev = input[i]
	}
	return inc
}

func part2(input []int) int {
	inc := 0
	prev := 0
	for i := 0; i < len(input)-2; i++ {
		if i == 0 {
			prev = input[0] + input[1] + input[2]
		}
		tmp := input[i] + input[i+1] + input[i+2]
		if prev < tmp {
			inc++
		}
		prev = tmp
	}
	return inc
}

func main() {
	f, _ := os.ReadFile("input.txt")
	input := []int{}
	for _, line := range strings.Split(string(f), "\n") {
		num, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}
		input = append(input, num)
	}
	fmt.Println("Part 1:", part1(input))
	fmt.Println("Part 2:", part2(input))
}
