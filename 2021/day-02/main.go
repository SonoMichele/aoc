package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

type Instructions struct {
	Direction string
	Amount    int
}

func part1(instructions []Instructions) int {
	horizontal := 0
	depth := 0
	for _, instruction := range instructions {
		switch instruction.Direction {
		case "forward":
			horizontal += instruction.Amount
		case "up":
			depth -= instruction.Amount
		case "down":
			depth += instruction.Amount
		}
	}

	return horizontal * depth
}

func part2(instructions []Instructions) int {
	horizontal := 0
	depth := 0
	aim := 0
	for _, instruction := range instructions {
		switch instruction.Direction {
		case "forward":
			horizontal += instruction.Amount
			depth += aim * instruction.Amount
		case "up":
			aim -= instruction.Amount
		case "down":
			aim += instruction.Amount
		}
	}

	return horizontal * depth
}

func main() {
	f, _ := os.ReadFile("input.txt")
	input := []Instructions{}
	for _, line := range strings.Split(string(f), "\n") {
		inst := strings.Split(line, " ")
		dir := inst[0]
		amt, _ := strconv.Atoi(inst[1])
		input = append(input, Instructions{
			Direction: dir,
			Amount:    amt,
		})
	}
	fmt.Println("Part 1:", part1(input))
	fmt.Println("Part 2:", part2(input))
}
